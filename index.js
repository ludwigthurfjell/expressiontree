import {
  accumulatorValue,
  ops,
  updateCalculatedValues,
  createExpression,
  skipAndValueIndexFromExpressionsAndOperator,
} from "./utils";

/**
 * @typedef {object} ExpressionTreeNode
 */

/**
 *
 * @param {(number|ExpressionTreeNode)} rootValue
 * @returns {ExpressionTreeNode} this
 */
function ExpressionTreeNode(rootValue) {
  if (!(this instanceof ExpressionTreeNode)) {
    throw new Error("Constructor called as a function");
  }

  if (
    !(
      rootValue?.constructor?.name === "ExpressionTreeNode" ||
      typeof rootValue === "number"
    )
  ) {
    throw new Error("Invalid root value parameter");
  }

  this._expressions = [createExpression(null, rootValue)];

  return this;
}

/**
 * Adds an addition expression or number to the list of expressions for a node.
 * @param {(number|ExpressionTreeNode)} value
 * @returns {ExpressionTreeNode} this
 */
ExpressionTreeNode.prototype.Add = function Add(value) {
  this._expressions.push(createExpression(ops.add, value));
  return this;
};

/**
 * Adds a subtraction expression or number to the list of expressions for a node.
 * @param {(number|ExpressionTreeNode)} value
 * @returns {ExpressionTreeNode} this
 */
ExpressionTreeNode.prototype.Sub = function Sub(value) {
  this._expressions.push(createExpression(ops.sub, value));
  return this;
};

/**
 * Adds a division expression or number to the list of expressions for a node.
 * @param {(number|ExpressionTreeNode)} value
 * @returns {ExpressionTreeNode} this
 */
ExpressionTreeNode.prototype.Div = function Div(value) {
  this._expressions.push(createExpression(ops.div, value));
  return this;
};

/**
 * Adds a multiplication expression or number to the list of expressions for a node.
 * @param {(number|ExpressionTreeNode)} value
 * @returns {ExpressionTreeNode} this
 */
ExpressionTreeNode.prototype.Mult = function Mult(value) {
  this._expressions.push(createExpression(ops.mult, value));
  return this;
};

/**
 * @returns {number} Calculated value of the expression node
 */
ExpressionTreeNode.prototype.result = function result() {
  // If there's ever only one expression
  if (this._expressions.length === 1) {
    if (typeof this._expressions[0].value !== "number") {
      return this._expressions[0].value.result();
    } else {
      return this._expressions[0].value;
    }
  }

  const divMaps = skipAndValueIndexFromExpressionsAndOperator(
    {},
    {},
    this._expressions,
    ops.div
  );

  const multMaps = skipAndValueIndexFromExpressionsAndOperator(
    divMaps.skipIndexMap,
    divMaps.valueIndexMap,
    this._expressions,
    ops.mult
  );

  const substuff = skipAndValueIndexFromExpressionsAndOperator(
    multMaps.skipIndexMap,
    multMaps.valueIndexMap,
    this._expressions,
    ops.sub
  );

  const addMaps = skipAndValueIndexFromExpressionsAndOperator(
    substuff.skipIndexMap,
    substuff.valueIndexMap,
    this._expressions,
    ops.add
  );
  /**
   * All expressions are calculated.
   * Possible todo would be to set somewhere which operators
   * the tree node should handle.
   */
  return addMaps.valueIndexMap[0];
};

/**
 *
 * @returns {string} Human readable string representation of the expression node
 */
ExpressionTreeNode.prototype.toString = function toString() {
  return `(${this._expressions
    .map((e, i) => {
      if (i === 0) {
        return `${e.value}`;
      }
      const isMinus = typeof e.value === "number" && e.value < 0;
      return isMinus ? ` ${e.op} (${e.value})` : ` ${e.op} ${e.value}`;
    })
    .join("")})`;
};

export default ExpressionTreeNode;
