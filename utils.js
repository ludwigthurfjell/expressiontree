export const ops = { add: "+", sub: "-", div: "%", mult: "x" };

export function createExpression(op, value) {
  return Object.freeze({ op, value });
}

export function accumulatorValue(value) {
  if (typeof value !== "number") {
    value = value.result();
  }
  return value;
}

export function skipAndValueIndexFromExpressionsAndOperator(
  skipIndexMap,
  valueIndexMap,
  expressions,
  op
) {
  const nextSkipIndexMap = { ...skipIndexMap };
  const nextValueIndexMap = { ...valueIndexMap };

  const calculateLastValueIndex = function () {
    let lastValueIndex;
    let backtrack = 0;
    for (let i = 0; i < expressions.length; i++) {
      const expression = expressions[i];
      if (nextSkipIndexMap[i]) {
        continue;
      }

      if (expression.op === op) {
        lastValueIndex = i - 1;
        backtrack = lastValueIndex;
        while (i >= 0 && nextSkipIndexMap[backtrack]) {
          backtrack--;
          lastValueIndex -= 1;
        }
        break;
      }
    }
    return lastValueIndex;
  };

  let lastValueIndex = calculateLastValueIndex();

  if (lastValueIndex !== undefined) {
    for (let i = 0; i < expressions.length; i++) {
      if (nextSkipIndexMap[i]) {
        continue;
      }

      const expression = expressions[i];

      if (expression.op === op) {
        // Check if we need to update last value index before calculating
        const countOfOps = expressions.filter((e) => e.op === op).length;
        if (Math.abs(lastValueIndex - i) > 1 && countOfOps > 1) {
          lastValueIndex = calculateLastValueIndex();
        }

        // Value to base next calculations on
        const first =
          nextValueIndexMap[lastValueIndex] !== undefined
            ? nextValueIndexMap[lastValueIndex]
            : accumulatorValue(expressions[lastValueIndex].value);

        switch (op) {
          case ops.div:
            const denom =
              nextValueIndexMap[i] ?? accumulatorValue(expression.value);

            nextValueIndexMap[lastValueIndex] = first / denom;
            break;
          case ops.mult:
            nextValueIndexMap[lastValueIndex] =
              first *
              (nextValueIndexMap[i] ?? accumulatorValue(expression.value));
            break;
          case ops.sub:
            nextValueIndexMap[lastValueIndex] =
              first -
              (nextValueIndexMap[i] ?? accumulatorValue(expression.value));
            break;
          case ops.add:
            nextValueIndexMap[lastValueIndex] =
              first +
              (nextValueIndexMap[i] ?? accumulatorValue(expression.value));
            break;
        }
        nextSkipIndexMap[i] = true;
      }
    }
  }

  return { valueIndexMap: nextValueIndexMap, skipIndexMap: nextSkipIndexMap };
}
