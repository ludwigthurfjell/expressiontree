import { describe, expect, it } from "@jest/globals";
import ExpressionTreeNode from "./index";

describe("ExpressionTreeNode", () => {
  describe("addition", () => {
    it("1+1 result 2", () => {
      const expression = new ExpressionTreeNode(1).Add(1);
      const actual = expression.result();
      expect(actual).toEqual(2);
    });

    it("1+1 toString '(1 + 1)'", () => {
      const expression = new ExpressionTreeNode(1).Add(1);
      const actual = expression.toString();
      expect(actual).toEqual("(1 + 1)");
    });

    it("1+1+1 result 3", () => {
      const expression = new ExpressionTreeNode(1).Add(1).Add(1);
      const actual = expression.result();
      expect(actual).toEqual(3);
    });

    it("1+1+1 toString '(1 + 1 + 1)'", () => {
      const expression = new ExpressionTreeNode(1).Add(1).Add(1);
      const actual = expression.toString();
      expect(actual).toEqual("(1 + 1 + 1)");
    });

    it("1+-1 result 0", () => {
      const expression = new ExpressionTreeNode(1).Add(-1);
      const actual = expression.result();
      expect(actual).toEqual(0);
    });

    it("1+-1 toString '(1 + (-1))'", () => {
      const expression = new ExpressionTreeNode(1).Add(-1);
      const actual = expression.toString();
      expect(actual).toEqual("(1 + (-1))");
    });
  });

  describe("subtraction", () => {
    it("1-1 toString '(1 - 1)'", () => {
      const expression = new ExpressionTreeNode(1).Sub(1);
      const actual = expression.toString();
      expect(actual).toEqual("(1 - 1)");
    });

    it("1-1 result 0", () => {
      const expression = new ExpressionTreeNode(1).Sub(1);
      const actual = expression.result();
      expect(actual).toEqual(0);
    });

    it("1--1 result 2", () => {
      const expression = new ExpressionTreeNode(1).Sub(-1);
      const actual = expression.result();
      expect(actual).toEqual(2);
    });

    it("1--1 toString '(1 - (-1))'", () => {
      const expression = new ExpressionTreeNode(1).Sub(-1);
      const actual = expression.toString();
      expect(actual).toEqual("(1 - (-1))");
    });
  });

  describe("division", () => {
    it("4/2 result 2", () => {
      const expression = new ExpressionTreeNode(4).Div(2);
      const actual = expression.result();
      expect(actual).toEqual(2);
    });

    it("4/2 toString '(4 % 2)'", () => {
      const expression = new ExpressionTreeNode(4).Div(2);
      const actual = expression.toString();
      expect(actual).toEqual("(4 % 2)");
    });

    it("1/0 to result Infinity", () => {
      const expression = new ExpressionTreeNode(4).Div(0);
      const actual = expression.result();
      expect(actual).toEqual(Infinity);
    });
  });

  describe("multiplication", () => {
    it("2*2 value 4", () => {
      const expression = new ExpressionTreeNode(2).Mult(2);
      const actual = expression.result();
      expect(actual).toEqual(4);
    });

    it("2*2 toString '(2 x 2)'", () => {
      const expression = new ExpressionTreeNode(2).Mult(2);
      const actual = expression.toString();
      expect(actual).toEqual("(2 x 2)");
    });

    it("2*-2 value -4", () => {
      const expression = new ExpressionTreeNode(2).Mult(-2);
      const actual = expression.result();
      expect(actual).toEqual(-4);
    });

    it("2*-2 toString '(2 x (-2))'", () => {
      const expression = new ExpressionTreeNode(2).Mult(-2);
      const actual = expression.toString();
      expect(actual).toEqual("(2 x (-2))");
    });

    it("2*1.5 value 3", () => {
      const expression = new ExpressionTreeNode(2).Mult(1.5);
      const actual = expression.result();
      expect(actual).toEqual(3);
    });
  });

  describe("combinations", () => {
    it("4 + 4 / 2 result 6", () => {
      const expression = new ExpressionTreeNode(4).Add(4).Div(2);
      expect(expression.result()).toEqual(6);
    });

    it("4 - 4 / 2 result 2", () => {
      const expression = new ExpressionTreeNode(4).Sub(4).Div(2);
      expect(expression.result()).toEqual(2);
    });

    it("4 + 4 * -2 result -4", () => {
      const expression = new ExpressionTreeNode(4).Add(4).Mult(-2);
      expect(expression.result()).toEqual(-4);
    });

    it("4*2*5/5/2*2 result 2", () => {
      const expression = new ExpressionTreeNode(4)
        .Mult(2)
        .Mult(5)
        .Div(5)
        .Div(2)
        .Mult(2);
      expect(expression.result()).toEqual(8);
    });

    it("(4+1)*(5-1) result 20", () => {
      const expression = new ExpressionTreeNode(
        new ExpressionTreeNode(4).Add(1)
      ).Mult(new ExpressionTreeNode(5).Sub(1));
      expect(expression.result()).toEqual(20);
    });

    it("2*2+2*2/2-2*2 result 2", () => {
      const expression = new ExpressionTreeNode(2)
        .Mult(2)
        .Add(2)
        .Mult(2)
        .Div(2)
        .Sub(2)
        .Mult(2);

      expect(expression.result()).toEqual(2);
    });
  });

  describe("nested", () => {
    it("single length nested", () => {
      const e = new ExpressionTreeNode(new ExpressionTreeNode(5));
      expect(e.result()).toEqual(5);
    });

    it("nested tree with multiple operations toString", () => {
      const expression = new ExpressionTreeNode(
        new ExpressionTreeNode(2).Mult(
          new ExpressionTreeNode(4).Sub(new ExpressionTreeNode(1).Add(1))
        )
      )
        .Sub(new ExpressionTreeNode(3).Sub(4))
        .Div(new ExpressionTreeNode(-1).Mult(-2));

      expect(expression.toString()).toEqual(
        "((2 x (4 - (1 + 1))) - (3 - 4) % (-1 x (-2)))"
      );
    });

    it("2*(4-(1+1)) - (3-4) / (-1*-2) result 4.5", () => {
      const expression = new ExpressionTreeNode(
        new ExpressionTreeNode(2).Mult(
          new ExpressionTreeNode(4).Sub(new ExpressionTreeNode(1).Add(1))
        )
      )
        .Sub(new ExpressionTreeNode(3).Sub(4))
        .Div(new ExpressionTreeNode(-1).Mult(-2));
      expect(expression.result()).toEqual(4.5);
    });

    it("(7 + ((3 - 2) x 5)) % 6 toString ((7 + ((3 - 2) x 5)) % 6)", () => {
      const expression = new ExpressionTreeNode(
        new ExpressionTreeNode(7).Add(
          new ExpressionTreeNode(new ExpressionTreeNode(3).Sub(2)).Mult(5)
        )
      ).Div(6);
      const actual = expression.toString();
      expect(actual).toEqual("((7 + ((3 - 2) x 5)) % 6)");
    });

    it("(7 + ((3 - 2) x 5)) % 6 result 2", () => {
      const expression = new ExpressionTreeNode(
        new ExpressionTreeNode(7).Add(
          new ExpressionTreeNode(new ExpressionTreeNode(3).Sub(2)).Mult(5)
        )
      ).Div(6);
      const actual = expression.result();
      expect(actual).toEqual(2);
    });
  });
});
